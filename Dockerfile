FROM ubuntu:latest

RUN apt-get update -y && apt-get -y install python3-pip

COPY ./requirements.txt /python-app/requirements.txt
WORKDIR /python-app
# RUN pip install -r requirements.txt

# COPY ./first-python-webpage.py /python-app/first-python-webpage.py

# EXPOSE 8080

# ENTRYPOINT ["python2", "first-python-webpage.py"]
RUN pip3 install -r requirements.txt

COPY /first-python-webpage.py /python-app/first-python-webpage.py

EXPOSE 8080

ENTRYPOINT [ "python3", "first-python-webpage.py", "run", "--host=0.0.0.0"]
